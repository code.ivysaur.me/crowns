package main

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlayRound(t *testing.T) {

	entropy := rand.New(rand.NewSource(0xdeadbeef))

	rr := NewRound(5, 4, entropy)
	nextPlayer, scores := rr.Play(0)

	assert.EqualValues(t, 0, nextPlayer)
	assert.EqualValues(t, []int{0, 36, 6, 26}, scores)
}

func TestPlayRound2(t *testing.T) {

	entropy := rand.New(rand.NewSource(1704595947230690673))

	rr := NewRound(3, 4, entropy)
	nextPlayer, scores := rr.Play(0)

	assert.EqualValues(t, 1, nextPlayer)
	assert.EqualValues(t, []int{21, 0, 16, 26}, scores)
}

func TestDrawDiscard(t *testing.T) {

	entropy := rand.New(rand.NewSource(0xdeadbeef))

	rr := NewRound(5, 4, entropy)

	assert.EqualValues(t, []Card{50, 47, 24, 25, 22}, rr.hands[0])

	c := rr.DrawFromDeck()
	assert.EqualValues(t, Card(15), c)

	rr.AddToHand(0, c)
	assert.EqualValues(t, []Card{50, 47, 24, 25, 22, 15}, rr.hands[0])

	rr.Discard(0, 2)
	assert.EqualValues(t, []Card{50, 47, 15, 25, 22}, rr.hands[0])

	assert.EqualValues(t, Card(24), rr.PeekFromDiscard())

	c = rr.DrawFromDiscard()
	rr.AddToHand(0, c)
	assert.EqualValues(t, []Card{50, 47, 15, 25, 22, 24}, rr.hands[0])

}
