# crowns

AI and simulator for the card game [Five Crowns](https://setgame.com/sites/default/files/instructions/FIVE%20CROWNS%20-%20ENGLISH_2.pdf).

## Usage

```bash
$ /usr/bin/time ./crowns > example.log
132.35user 2.95system 1:21.78elapsed 165%CPU (0avgtext+0avgdata 112348maxresident)k
0inputs+176outputs (0major+1031510minor)pagefaults 0swaps

$ tail -n13 example.log 
P2 taking  3★ from the deck
P2 discards 10♠ (position 7)
P3 taking 10♠ from the discard pile
P3 discards  5♦ (position 6)
Found 306698 possible groupings
P0 has [ [[ K♣  4★  4♣] [ K♠  5♠  5♥] [ 6♣  7♣  8♣] [ 6★ Jok  K♣  9★]] leftover [] ] (score: 0)
Found 12716 possible groupings
P1 has [ [[ K♥  3♥  3★] [ 5♦  K★  7♦] [Jok 10♦  J♦] [ 7★  8★  9★]] leftover [ 6♥] ] (score: 6)
P2 has [ [[ 3♦  3★  3♥] [ 4♠  4♦  4★]] leftover [ 9♥  6♦  5♠  5★  9♦  7♣  8♦] ] (score: 49)
P3 has [ [[ K★  6♦  6★] [10♥ 10♠ 10★] [ 9♣ Jok  J♣  Q♣]] leftover [ 4♣  4♥  3♠] ] (score: 11)
The round has ended
Running total scores: [105 102 158 154]
The game has ended
```

## Status

- [X] Find books and runs in hands including wild cards
- [X] Tree search to find best combination with lowest outstanding score
	- [ ] More optimized tree pruning for very large number of wildcards
- [X] Arbitrary player count
- [X] Unit test coverage
- [X] Pluggable strategy architecture
- [X] Minmax AI strategy
- [ ] More advanced strategies
	- [ ] Take what other players are drawing into account, and avoid discarding them
	- [ ] Avoid discarding wildcards for other players
	- [ ] Identify what cards would complete a book or run and their drawing odds
	- [ ] Oracle / "Cheating" strategy
- [ ] Interactive player
	- [ ] Graphical UI
- [ ] Solitaire mode with single deck
- [ ] Monte carlo analysis on game properties
	- [ ] Average number of turns per round
	- [ ] Odds of the starting player winning
