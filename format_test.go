package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormatHandGrouping(t *testing.T) {
	str := FormatHandGroupings([]Card{5, 10, 15, 20}, [][]int{[]int{0, 1}, []int{2}})
	assert.EqualValues(t, "[ [[ 1♠  2♠] [ 3♠]] leftover [ 4♠] ]", str)
}
