package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"
)

func main() {
	numPlayers := flag.Int("Players", 4, "Number of CPU players")
	seed := flag.Int64("Seed", time.Now().UnixNano(), "Random game seed")
	flag.Parse()

	entropy := rand.New(rand.NewSource(*seed))

	fmt.Printf("Random seed: %d\n", *seed)

	PlayGame(*numPlayers, entropy)
}
