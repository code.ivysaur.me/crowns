package main

import (
	"fmt"
)

func FormatHandGroupings(hand []Card, groupings [][]int) string {
	tmp := forkHand(hand)

	cgroups := [][]Card{}
	for _, group := range groupings {
		cgroup := []Card{}
		for _, cidx := range group {
			cgroup = append(cgroup, hand[cidx])

			tmp[cidx] = NewMasked()
		}
		cgroups = append(cgroups, cgroup)
	}

	leftover := []Card{}
	for _, cv := range tmp {
		if !cv.Masked() {
			leftover = append(leftover, cv)
		}
	}

	return fmt.Sprintf("[ %v leftover %v ]", cgroups, leftover)
}
