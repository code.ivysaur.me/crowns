package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindBooks(t *testing.T) {

	hand := []Card{15, 16, 17} // [ 3♠  3♥  3♦]

	t.Logf("Finding books in hand: %v", hand)

	p := MakeBooks(hand, 10)
	assert.EqualValues(t, [][]int{[]int{0, 1, 2}}, p)
}

func TestFindRuns(t *testing.T) {
	hand := []Card{15, 20, 25, 25, 30} // [ 3♠  4♠  5♠  5♠  6♠]

	t.Logf("Finding runs in hand: %v", hand)

	p := MakeRuns(hand, 10)
	assert.EqualValues(t, [][]int{[]int{0, 1, 2}, []int{0, 1, 2, 4}, []int{0, 1, 3}, []int{0, 1, 3, 4}, []int{1, 2, 4}, []int{1, 3, 4}}, p)
}

func TestFindMultiGroups(t *testing.T) {
	hand := []Card{15, 20, 25, 30, 16, 17}

	t.Logf("Finding all groupups in hand: %v", hand)

	p := MakeMultiGroups(hand, 10)
	assert.EqualValues(t, [][][]int{[][]int{[]int{0, 4, 5}}, [][]int{[]int{0, 4, 5}, []int{1, 2, 3}}, [][]int{[]int{0, 1, 2}}, [][]int{[]int{0, 1, 2, 3}}, [][]int{[]int{1, 2, 3}}, [][]int{[]int{1, 2, 3}, []int{0, 4, 5}}}, p)
}

func TestSolveHand(t *testing.T) {
	hand := []Card{15, 20, 25, 30, 16, 17}

	t.Logf("Finding best groupups in hand: %v", hand)

	grouping, score := FindBestGrouping(hand, 10)
	assert.EqualValues(t, [][]int{[]int{0, 4, 5}, []int{1, 2, 3}}, grouping)
	assert.EqualValues(t, 0, score)
}

func TestScoreHand(t *testing.T) {
	// P2 has [ [[ J★  8♥  8★] [ 9♥  9♥  9♠] [ Q♠ Jok  Q♠]] leftover [10♣  4♣] ] (score: 100)

	hand := []Card{15, 20, 25, 30, 16, 17, 55, 60}

	t.Logf("Finding score for hand: %v", hand)

	grouping, score := FindBestGrouping(hand, 10)
	assert.EqualValues(t, [][]int{[]int{0, 4, 5}, []int{1, 2, 3}}, grouping)
	assert.EqualValues(t, 23, score)
}
