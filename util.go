package main

func search(hand []Card, wildFace int, search Card) []int {
	ret := []int{}
	for idx, c := range hand {
		if c.Masked() {
			continue // Doesn't match anything
		}

		if c == search || c.Joker() || c.Face() == wildFace {
			ret = append(ret, idx)
		}
	}
	return ret
}

func forkHand(hand []Card) []Card {
	ret := make([]Card, len(hand))
	copy(ret, hand)
	return ret
}
